import gym
import numpy as np

from SI_keras_functions import *
from tensorflow.keras.optimizers import Adam
from rl.callbacks import FileLogger, ModelIntervalCheckpoint

env_name = 'SpaceInvaders-v0'
env = gym.make(env_name)
height, width, channels = env.observation_space.shape
actions = env.action_space.n
np.random.seed(123)
env.seed(123)

# model and training decisions
warmup = 1000
steps_train = 1000000

# build model
model = build_model(height, width, channels, actions)

# build and compile agent
dqn = build_agent(model, actions, warmup)
dqn.compile(Adam(lr=1e-4))


# Okay, now it's time to learn something! We capture the interrupt exception so that training
# can be prematurely aborted. Notice that now you can use the built-in tensorflow.keras callbacks!
weights_filename = f'dqn_{env_name}_{str(steps_train)}_weights.h5f'
checkpoint_weights_filename = 'dqn_' + env_name + '_weights_{step}.h5f'
log_filename = f'dqn_{env_name}_log.json'
callbacks = [ModelIntervalCheckpoint(checkpoint_weights_filename, interval=2500)]
callbacks += [FileLogger(log_filename, interval=1000)]
# train agent
dqn.fit(env, callbacks=callbacks, nb_steps=steps_train, log_interval=1000, visualize=False, verbose=2)

# After training is done, we save the final weights one more time.
dqn.save_weights(weights_filename, overwrite=True)
