import gym
import numpy as np

from SI_keras_functions import *
from tensorflow.keras.optimizers import Adam

env_name = 'SpaceInvaders-v0'
env = gym.make(env_name)
height, width, channels = env.observation_space.shape
actions = env.action_space.n
np.random.seed(123)
env.seed(123)

# model and training decisions
warmup = 1000
episodes_test = 10

# build model
model = build_model(height, width, channels, actions)

# build and compile agent
dqn = build_agent(model, actions, warmup)
dqn.compile(Adam(lr=1e-4))

weights_filename = f'dqn_{env_name}_{str(steps_train)}_weights.h5f'
dqn.load_weights(weights_filename)

scores = dqn.test(env, nb_episodes=episodes_test, visualize=True)
print(np.mean(scores.history['episode_reward']))
