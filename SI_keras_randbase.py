import gym
import random
import numpy as np
import json


env = gym.make('SpaceInvaders-v0')
height, width, channels = env.observation_space.shape
actions = env.action_space.n

all_scores = []
all_steps = []

def baseline():
    episodes = 10
    base_score = []
    base_steps = []
    for episode in range(1, episodes + 1):
        state = env.reset()
        done = False
        score = 0
        steps=0
        while not done:
            #env.render()
            action = random.choice([0, 1, 2, 3, 4, 5])
            n_state, reward, done, info = env.step(action)
            score += reward
            steps += 1
        #print('Episode:{} Score:{}'.format(episode, score))
        base_score.append(score)
        base_steps.append(steps)
    #env.close()

    return base_score, base_steps

base_score, base_steps = baseline()

f_base_score = []
f_base_steps = []
i=0

while i <= 142:
    base_score, base_steps = baseline()
    f_base_score.append(base_score)
    f_base_steps.append(base_steps)
    i += 1

filename_scores = f'all_base_scores.json'
with open(filename_scores, 'w') as f:
    # indent=2 is not needed but makes the file human-readable
    json.dump(f_base_score, f, indent=2)
filename_steps = f'all_base_steps.json'
with open(filename_steps, 'w') as f:
    # indent=2 is not needed but makes the file human-readable
    json.dump(f_base_steps, f, indent=2)



