## Space Invaders:
In this project, a deep q learning agent is created and trained to play the atari game "Space Invaders"

## Required installations:
tensorflow version 2.3.1
gym 
gym[atari]
keras-rl2

## Agent and model:
The code for the deep q learning agent and the deep learning model can be found in the script: 
SI_keras_functions.py, the agent is build using the Keras RL library and the convolutional 
neural network is build using Keras

## Baseline:
SI_keras_randbase contains a baseline, choosing random actions from the action space. 
The reward and steps are saved in two separate .json files. 

## Training:
SI_keras_agent_train.py contains the training session for the model. 
Environment "SpaceInvaders-v0" is defined from gym.
Build using keras functions.
The weights of the model are saved in a given interval to be used later.

## Testing:
Script for testing is SI_test_save_loop.py. Initializes the "spaceInvaders-v0" environment from gym. 
loops over the saved models in a given interval, loading their weights from the files saved in training. 
Appends the reward and steps for the tests, and saves them in two separate .josn files. 
The baseline results are obtained running the SI_keras_agent_train.py script with a while
loop running until 141. 

## Results:
### Visualisation 
Created running the SI_plots_notebook.ipynb script using the 
all_scores_until_1000000.json, all_steps_until_1000000.json, all_base_scores.json and all_base_steps.json files. 


## References
The code for this project is primarily from:
https://github.com/nicknochnack/KerasRL-OpenAI-Atari-SpaceInvadersv0
With some modifications.






