import gym
import numpy as np
import json

from SI_keras_functions import *
from tensorflow.keras.optimizers import Adam

env_name = 'SpaceInvaders-v0'
env = gym.make(env_name)
height, width, channels = env.observation_space.shape
actions = env.action_space.n
np.random.seed(123)
env.seed(123)

# test different models
# steps up to 20.000: range(1,9)
# steps up to 350.000: range(1,141)
steps = [i*2500 for i in range(1, 141)]
# add the 1 million weights
steps.append(1000000)

all_scores = []
all_steps = []

# loop over all the saved weight files
for i, steps_train in enumerate(steps):
    # number of tests
    episodes_test = 10
    # just sat to the same as the training model
    warmup = 1000

    # build model
    model = build_model(height, width, channels, actions)

    # build and compile agent
    dqn = build_agent(model, actions, warmup)
    dqn.compile(Adam(lr=1e-4))

    # compute filename and load weights
    weights_filename = f'dqn_{env_name}_weights_{str(steps_train)}.h5f'
    dqn.load_weights(weights_filename)

    # test the agent
    scores = dqn.test(env, nb_episodes=episodes_test, visualize=False)
    # append the scores
    all_scores.append(scores.history['episode_reward'])
    all_steps.append(scores.history['nb_steps'])
    # print the status for the model
    print('model: {} mean score: {}'.format(steps_train, np.mean(scores.history['episode_reward'])))
    print()
    # save the scores for the first model, the last model and every 10 models
    if i == 1 or i // 10 == i/10 or i == 141:
        filename_scores = f'all_scores_until_{str(steps_train)}.json'
        with open(filename_scores, 'w') as f:
            # indent=2 is not needed but makes the file human-readable
            json.dump(all_scores, f, indent=2)
        filename_steps = f'all_steps_until_{str(steps_train)}.json'
        with open(filename_steps, 'w') as f:
            # indent=2 is not needed but makes the file human-readable
            json.dump(all_steps, f, indent=2)
